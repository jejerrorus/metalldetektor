package com.example.administrator.metalldetektor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.FloatMath;
import android.widget.ProgressBar;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private ProgressBar progressBar;
    private TextView percentagetext;

    private SensorManager sensorManager;
    private Sensor sensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create our Sensor manger
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        // Create our Magnetic Field sensor
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        // Register our Sensor Listener
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);

        // Assign TextView
        percentagetext = (TextView) findViewById(R.id.percentage);

        // Assign ProgessBar
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Not in use
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Pause the sensor if the app is paused
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] mag = event.values;
        double betrag = Math.sqrt(mag[0] * mag[0] + mag[1] * mag[1] + mag[2] * mag[2]);

        betrag -=15;

        if(betrag>100) {
            betrag = 100;
        }


        percentagetext.setText("Stärke: " + Math.round(betrag));
        progressBar.setProgress((int) betrag);
    }


}
